//---------------------------------------------------------------------------
// String that names the compiler being used
//---------------------------------------------------------------------------
#define BOOST_COMPILER "TI Compiler Version " BOOST_STRINGIZE(__TI_COMPILER_VERSION__)

//---------------------------------------------------------------------------
// TI uses the EDG compiler front end to parse C/C++ code.  The name
// __EDG_VERSION__ is pre-defined by TI compilers, but only in the latest
// releases.  303 is what is most commonly used by TI compilers that
// do not predefine __EDG_VERSION__.  Inherit all the other settings for the
// EDG front end from the Boost file already here.
//---------------------------------------------------------------------------
#if !defined(__EDG_VERSION__)
#define __EDG_VERSION__ 303
#endif
#include <boost/config/compiler/common_edg.hpp>

//---------------------------------------------------------------------------
// __EXCEPTIONS is true only if the switch --exceptions is used.  Most Boost
// code requires use of --exceptions to build correctly.
//---------------------------------------------------------------------------
#if !defined(__EXCEPTIONS)
#define BOOST_NO_EXCEPTIONS
#endif

//---------------------------------------------------------------------------
// Our library implementation only supplies the C standard locale.  No
// other locales are available.  Other such settings likely need to be
// added to this file.
//---------------------------------------------------------------------------
#define BOOST_NO_STD_LOCALE
#define BOOST_NO_INCLASS_MEMBER_INITIALIZATION
